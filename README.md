# 4. Gestion des ressources de conteneur
------------


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang">

>Carlin FONGANG | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

#  Introduction à la gestion des ressources des conteneurs

Bonjour et bienvenue dans cette leçon où nous allons parler de la gestion des ressources des conteneurs. Voici un bref aperçu des sujets que nous allons aborder :

1. Les demandes de ressources
2. Les limites de ressources
3. Une démonstration pratique

### Demandes de ressources

Qu'est-ce qu'une demande de ressources ? Les demandes de ressources vous permettent de définir une quantité de ressources, comme le CPU et la mémoire, que vous attendez que votre conteneur utilise. Cela permet au planificateur Kubernetes de prendre ces demandes en compte lors de la programmation des pods sur les différents nœuds.

Le planificateur Kubernetes examine les demandes de ressources. Par exemple, si votre demande de ressources requiert 5 gigaoctets de mémoire et qu'aucun nœud ne dispose de cette quantité de mémoire disponible, le planificateur cherchera un autre nœud ayant suffisamment de mémoire avant de programmer le pod sur celui-ci.

```yaml
apiVersion: v1
kind: Pod
metadata:
    name: my-pod
spec:
    containers:
    - name: busybox
    image: busybox
    resources:
        requests:
        cpu: "250m"
        memory: "128Mi"
```

Il est important de noter que les conteneurs peuvent utiliser plus ou moins de ressources que celles demandées. Les demandes de ressources affectent uniquement la planification, elles ne forcent pas le conteneur à rester dans cette limite. Voici un exemple de ce à quoi ressemble une demande de ressources dans la spécification d'un conteneur. Pour les demandes de ressources, la mémoire est mesurée en octets et le CPU en unités de CPU, qui sont des millièmes de CPU (1/1000 d'un CPU).



### Limites de ressources

Les limites de ressources permettent de limiter la quantité de ressources que nos conteneurs peuvent utiliser, empêchant ainsi nos conteneurs de consommer plus de ressources qu'ils ne le devraient. Il est important de noter que lorsqu'on utilise des limites de ressources, le runtime du conteneur est responsable de l'application de ces limites, et différents runtimes appliquent ces limites de différentes manières.

Par exemple, certains runtimes peuvent appliquer les limites de ressources en terminant le processus du conteneur. Ainsi, en fonction du runtime utilisé, si vous définissez des limites de ressources, vos conteneurs peuvent être arrêtés s'ils tentent d'utiliser plus de ressources que celles spécifiées dans la limite de ressources. Voici un exemple de spécification des limites de ressources dans un conteneur.

```yaml
apiVersion: v1
kind: Pod
metadata:
    name: my-pod
spec:
    containers:
    - name: busybox
    image: busybox
    resources:
        requests:
        cpu: "250m"
        memory: "128Mi"
```

### Démonstration pratique

Passons maintenant à une démonstration pratique pour voir à quoi cela ressemble dans notre cluster Kubernetes.

1. **Création d'un pod avec une demande de ressources élevée** :
   
   Je vais créer un pod appelé `big-request-pod` avec une demande de ressources très élevée. Voici une définition de pod basique, utilisant BusyBox et exécutant une boucle de sommeil. J'ajoute une demande de ressources de 10 000m CPUs (soit 10 CPUs) et 128 mégaoctets de mémoire. Comme mon serveur ne dispose pas de 10 CPUs, le pod restera en état `Pending` car Kubernetes ne trouvera pas de nœud pouvant satisfaire cette demande.

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: big-request-pod
   spec:
     containers:
     - name: busybox
       image: busybox
       command: ["sh", "-c", "sleep 3600"]
       resources:
         requests:
           cpu: "10000m"
           memory: "128Mi"
   ```

2. **Création d'un pod avec des demandes et des limites de ressources raisonnables** :

   Créons maintenant un pod appelé `resource-pod` avec des demandes et des limites de ressources plus raisonnables. Voici la définition de pod :

   ```yaml
   apiVersion: v1
   kind: Pod
   metadata:
     name: resource-pod
   spec:
     containers:
     - name: busybox
       image: busybox
       command: ["sh", "-c", "sleep 3600"]
       resources:
         requests:
           cpu: "250m"
           memory: "128Mi"
         limits:
           cpu: "500m"
           memory: "256Mi"
   ```

   Ce pod devrait être correctement programmé et démarré car les demandes de ressources sont raisonnables.

### Conclusion

Dans cette leçon, nous avons parlé des demandes de ressources, qui permettent de fournir une estimation des ressources que nos conteneurs vont utiliser, affectant ainsi la planification. Nous avons également discuté des limites de ressources, qui permettent d'imposer des limites sur la quantité de ressources que nos conteneurs peuvent utiliser. Enfin, nous avons réalisé une démonstration pratique de ces concepts.

C'est tout pour cette leçon. À la prochaine !

# Reférences



https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/#resource-requests-and-limits-of-pod-and-container